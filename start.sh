#!/bin/bash
echo "Start Docker"
docker-compose up -d

echo "Build"
docker-compose build

echo "Composer Update"
composer update

echo "Composer Install"
composer install

echo "Import DB"
docker-compose run php bin/console doctrine:schema:update --force