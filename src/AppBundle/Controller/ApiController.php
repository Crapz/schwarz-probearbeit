<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Imprint;

/**
 * @Route("/api", name="home")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/get")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getImprint(Request $request)
    {
        // Load repository
        $repo = $this->getDoctrine()->getRepository('AppBundle:Imprint');
        $imprint = $repo->findOneByName("en");

        // Building Array
        $data = [
            "lang" => $request->getLocale(),
            "address" => $imprint->getAdress(),
            "phone" => $imprint->getPhone(),
            "email" => $imprint->getEmail()
        ];

        // Return JSON
        return $this->json($data);
    }
}