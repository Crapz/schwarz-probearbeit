<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use GuzzleHttp\Client;

class UsersController extends AbstractController
{
    /**
     * @return Response
     * @Route("/benutzer", name="users")
     * @Route("/users/", options={"i18n"=false})
     */
    public function show()
    {
        return $this->render('pages/users.html.twig', [
            "users" => $this->getUsers()
        ]);
    }

    /**
     * Get Users from API
     *
     * @return mixed
     */
    private function getUsers()
    {
        $client = new Client(['base_uri' => 'https://jsonplaceholder.typicode.com/']);
        $response = $client->request('GET', 'users');
        return json_decode($response->getBody()->getContents());
    }
}