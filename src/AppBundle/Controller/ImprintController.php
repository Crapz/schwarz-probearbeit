<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Imprint;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ImprintController extends AbstractController
{
    /**
     * @Route("/impressum", name="imprint")
     * @param Request $request
     * @return Response
     */
    public function show(Request $request)
    {

        // Get Imprint
        $imprint = $this->getCurrentImprint($request->getLocale());

        // Return page with imprint
        return $this->render('pages/imprint.html.twig',
            [
                // If no imprint for the current localization exists, give an error
                "imprint" => $imprint ?? false
            ]);
    }

    /**
     * @Route("/editImprint", name="editImprint", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();

        // Content from AJAX Post
        $content = json_decode($request->getContent());

        // Field
        $field = $content[0];

        // Value
        $value = $content[1];

        // Get Current Imprint
        $imprint = $this->getCurrentImprint($request->getLocale());

        // If there's no current Imprint
        if (empty($imprint)) {

            // Create new imprint
            $imprint = new Imprint();

            // Set locale as name
            $imprint->setName($request->getLocale());
        }

        // Depending on which field changed ...
        switch($field) {

            // .. change address
            case "adress":
                $imprint->setAdress($value);
                break;

            // ... change email
            case "email":
                $imprint->setEmail($value);
                break;

            // ... change phone
            case "phone":
                $imprint->setPhone($value);
        }

        // Save!
        $entityManager->persist($imprint);
        $entityManager->flush();

        // Return success
        return $this->json(['success' => true]);
    }

    /**
     * @param $lang
     * @return mixed
     */
    private function getCurrentImprint($lang)
    {
        // Load repository
        $repo = $this->getDoctrine()->getRepository('AppBundle:Imprint');

        // Find & return correct imprint
        return $repo->findOneByName($lang);
    }
}